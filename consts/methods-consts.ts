export const POST: string = 'POST'
export const GET: string = 'GET'
export const DELETE: string = 'DELETE'
export const HEAD: string = 'HEAD'
export const PUT: string = 'PUT'
export const CONNECT: string = 'CONNECT'
export const OPTIONS: string = 'OPTIONS'
export const TRACE: string = 'TRACE'
export const PATCH: string = 'PATCH'
