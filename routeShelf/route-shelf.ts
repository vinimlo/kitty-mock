import IRoute from '../interfaces/IRoute'
import IRouteShelf from '../interfaces/IRouteShelf'

export default class RouteShelf implements IRouteShelf {
  private routesList: IRoute[] = []

  public getItem (path: string, method: string): Promise<IRoute> {
    return new Promise((resolve, reject) => {
      let routes: IRoute[] = this.filterRoutesByPath(this.routesList, path)
      if (routes.length == 0) {
        return reject(404)
      }
      let route: IRoute = this.getRouteByMethod(routes, method.toUpperCase())
      if (route) {
        return resolve(route)
      }
      reject(405)
    })
  }

  public getItems (): IRoute[] {
    return this.routesList
  }

  public setItem (route: IRoute): boolean {
    route.filters.method = route.filters.method.toUpperCase()
    let routeItem: IRoute = this.getRouteByPathAndMethod(this.routesList, route.filters)
    if (!routeItem) {
      this.routesList.push(route)
    }
    return !routeItem
  }
  public updateItem (routePath: string, routeMethod: string, route: IRoute): boolean {
    route.filters = {path: routePath, method: routeMethod.toUpperCase()}
    let routeItem: IRoute = this.getRouteByPathAndMethod(this.routesList, route.filters)
    if (routeItem) {
      this.routesList = this.filterPathAndMethodDifferentsRoutes(this.routesList, {
        path: route.filters.path,
        method: route.filters.method
      })
      this.routesList.push(route)
    }
    return !!routeItem
  }

  public removeItem (routePath: string, routeMethod: string): boolean {
    let routeItem: IRoute = this.getRouteByPathAndMethod(this.routesList, {
      path: routePath,
      method: routeMethod.toUpperCase()
    })
    if (routeItem) {
      this.routesList = this.filterPathAndMethodDifferentsRoutes(this.routesList, {
        path: routePath,
        method: routeMethod.toUpperCase()
      })
    }
    return !!routeItem
  }

  private filterRoutesByPath (routesList: IRoute[], path: string): IRoute[] {
    return routesList.filter((route) => route.filters.path == path)
  }

  private filterPathAndMethodDifferentsRoutes (routesList: IRoute[], { path, method }): IRoute[] {
    return routesList.filter((route) => route.filters.path != path || route.filters.method != method)
  }

  private getRouteByPathAndMethod (routesList: IRoute[], { path, method }): IRoute {
    return routesList.find((routeItem) => routeItem.filters.path == path && routeItem.filters.method == method)
  }

  private getRouteByMethod (routesList: IRoute[], method): IRoute {
    return routesList.find((routeItem) => routeItem.filters.method == method)
  }
}