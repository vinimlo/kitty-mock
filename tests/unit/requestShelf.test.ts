import { expect } from 'chai'
import 'mocha'
import RequestShelf from '../../requestShelf/request-shelf'
import IRequest from '../../interfaces/IRequest'
import { GET } from '../../consts/methods-consts'
import IRequestShelf from '../../interfaces/IRequestShelf'

describe('Request Shelf', () => {

  it('inserts and getting item from request shelf', () => {
    const requestShelf = new RequestShelf()
    let request: IRequest = getMockRequest()
    requestShelf.setRequest('GET/oi', request)
    getAndCheckRequests(requestShelf, JSON.stringify([request]))
  })
  it('gets deleted requests from a mocker', () => {
    const requestShelf = new RequestShelf()
    let request: IRequest = getMockRequest()
    requestShelf.setRequest('GET/oi', request)
    getAndCheckRequests(requestShelf, JSON.stringify([request]))
    requestShelf.deleteRequests('GET/oi')
    getAndCheckRequests(requestShelf, '[]')
  })
  it('gets deleted requests from a mocker, setting and getting again', () => {
    const requestShelf = new RequestShelf()
    let request: IRequest = getMockRequest()
    requestShelf.setRequest('GET/oi', request)
    getAndCheckRequests(requestShelf, JSON.stringify([request]))
    requestShelf.deleteRequests('GET/oi')
    getAndCheckRequests(requestShelf, '[]')
    requestShelf.setRequest('GET/oi', request)
    getAndCheckRequests(requestShelf, JSON.stringify([request]))
  })
  it('gets requests from a empty mocker', () => {
    const requestShelf = new RequestShelf()
    getAndCheckRequests(requestShelf, '[]')
  })
})

function getMockRequest (): IRequest {
  return {
    ip: '127.0.0.1',
    header: undefined,
    body: 'oi',
    method: GET,
    url: '/oi',
    date: new Date().toString()
  }
}

function getAndCheckRequests (requestShelf: IRequestShelf, expected: string) {
  let request: IRequest[] = requestShelf.getRequests('GET/oi')
  expect(JSON.stringify(request)).to.equal(expected)
}

