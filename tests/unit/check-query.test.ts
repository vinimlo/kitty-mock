import { expect } from 'chai'
import { checkQuery } from '../../helpers/check-query'
import { parse } from 'url'
import { ParsedUrlQuery } from 'querystring'

describe('Check query', () => {
  let url = require('url')
  it('Check valid query', () => {
    let query: ParsedUrlQuery = parse('/=^.^=/route?path=/oi&method=GET', true).query
    expect(checkQuery(query)).equal('')
  })
  it('Check invalid query, missing method', () => {
    let query: ParsedUrlQuery = parse('/=^.^=/route?path=/oi', true).query
    expect(checkQuery(query)).equal('request query missing method')
  })
  it('Check invalid query, missing path', () => {
    let query: ParsedUrlQuery = parse('/=^.^=/route?method=GET', true).query
    expect(checkQuery(query)).equal('request query missing path')
  })
})