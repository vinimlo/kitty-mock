import getJsend from '../helpers/get-jsend'
import { IncomingMessage } from 'http'
import IResponse from '../interfaces/IResponse'
import IHttpMocker from '../interfaces/IHttpMocker'
import IWs from '../interfaces/IWs'
import IQueryResponse from '../interfaces/IQueryResponse'
import getAndCheckQuery from '../helpers/get-and-check-query'

/**
 * @api {delete} :port/=^.^=/route Delete a websocket route
 * @apiGroup HttpMocker
 * @apiVersion 0.4.0
 * @apiName DeleteWsRoute
 * @apiDescription It deletes a websocket route.
 *
 * @apiParam {String} path    The route path. It musts be a query param.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": "fail",
 *       "message": "route does not exist"
 *     }
 */

export default class DeleteWsRouteHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise(async (resolve) => {
      let response: IQueryResponse = getAndCheckQuery(req)
      if (response.err)
        return resolve({
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: response.err })
        })
      let ws: IWs = this.mocker.getWsShelf().getWs(response.query.path)
      if (!ws) {
        return resolve({
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: 'route does not exist' })
        })
      }
      if (ws.routeWs.socket) {
        ws.routeWs.socket.close()
      }
      if (ws.routePairWs.socket) {
        ws.routePairWs.socket.close()
      }
      ws.routeWs.server.close()
      ws.routePairWs.server.close()
      this.mocker.getWsShelf().deleteWs(response.query.path)
      resolve({
        code: 204,
        body: getJsend({ statusCode: 204, data: undefined, message: undefined })
      })
    })
  }
}

