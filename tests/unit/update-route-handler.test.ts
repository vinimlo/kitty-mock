import { expect } from 'chai'
import 'mocha'
import IResponse from '../../interfaces/IResponse'
import axios from 'axios'
import { createServer, Server } from 'http'
import IJsend from '../../interfaces/IJsend'
import HttpMocker from '../../mocker/httpMocker'
import IRoute from '../../interfaces/IRoute'
import UpdateRouteHandler from '../../handlers/update-route-handler'

describe('Update route handler', () => {

  it('updates an existing route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new UpdateRouteHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', undefined, undefined, 204)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    let data: IRoute = getMockRoute()
    data.filters = undefined
    data.response = data.response as IResponse
    data.response.code = 201
    return axios.patch('http://127.0.0.1:7003?path=/oi&method=get', data).finally(async () => {
      expect(success).to.equal(1)
      let shelf = mocker.getRouteShelf()
      shelf.getItem('/oi', 'GET')
      expect(JSON.stringify(await mocker.getRouteShelf().getItem('/oi', 'GET'))).to.equal('{"response":{"code":201,"body":"oioi"},"filters":{"path":"/oi","method":"GET"}}')
      return server.close()
    })
  })
  it('updates an existing route with invalid data', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new UpdateRouteHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'fail', undefined, 'request with invalid route response code', 400)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    let data: IRoute = getMockRoute()
    data.filters = undefined
    data.response = data.response as IResponse
    data.response.code = 800
    return axios.patch('http://127.0.0.1:7003?path=/oi&method=get', data).finally(async () => {
      expect(success).to.equal(1)
      let shelf = mocker.getRouteShelf()
      shelf.getItem('/oi', 'GET')
      expect(JSON.stringify(await mocker.getRouteShelf().getItem('/oi', 'GET'))).to.equal('{"filters":{"path":"/oi","method":"GET"},"response":{"code":200,"body":"oioi"}}')
      return server.close()
    })
  })
  it('updates a non existing route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new UpdateRouteHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'fail', undefined, 'route does not exist', 400)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    let data: IRoute = getMockRoute()
    data.filters = undefined
    return axios.patch('http://127.0.0.1:7003?path=/oi&method=get', data).finally(async () => {
      expect(success).to.equal(1)
      mocker.getRouteShelf().getItem('/oi', 'GET').catch((code) => {
        expect(code).to.equal(404)
      })
      return server.close()
    })
  })
  it('updates an existing route with filter in update', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new UpdateRouteHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'fail', undefined, 'filters cannot be updated', 400)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    return axios.patch('http://127.0.0.1:7003?path=/oi&method=get', getMockRoute()).finally(async () => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
})

async function checkResponse (response: IResponse, status: string, data: string, message: string, code: number) {
  let jsend: IJsend = JSON.parse(response.body)
  expect(jsend.status).to.equal(status)
  expect(JSON.stringify(jsend.data)).to.equal(data)
  expect(jsend.message).to.equal(message)
  expect(response.code).to.equal(code)
}

function getMockRoute (): IRoute {
  return {
    filters: { path: '/oi', method: 'GET' },
    response: { code: 200, body: 'oioi' }
  }
}
