export default interface IFilter {
  path: string
  method: string
}
