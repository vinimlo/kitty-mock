import getJsend from '../helpers/get-jsend'
import { IncomingMessage } from 'http'
import HttpMocker from '../mocker/httpMocker'
import { DELETE, GET, PATCH, POST } from '../consts/methods-consts'
import CreateRouteHandler from './create-route-handler'
import DeleteRouteHandler from './delete-route-handler'
import RoutesGetterHandler from './routes-getter-handler'
import MockerHealthCheckerHandler from './mocker-health-checker-handler'
import StopMockerHandler from './stop-mocker-handler'
import IResponse from '../interfaces/IResponse'
import HistoryGetterHandler from './history-getter-handler'
import ClearHistoryHandler from './clear-history-handler'
import getPort from '../helpers/get_port'
import { RESERVED_PATH } from '../consts/kitty'
import CreateWsRouteHandler from './create-ws-route-handler'
import DeleteWsRouteHandler from './delete-ws-route-handler'
import UpdateRouteHandler from './update-route-handler'
import RouteGetterHandler from './route-getter-handler'

/**
 * @api {post} /create Create a new HttpMocker

 * @apiGroup HttpMocker
 * @apiVersion 0.4.0
 * @apiName CreateHttpMocker
 * @apiDescription It creates a new HttpMocker.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} data HttpMocker details.
 * @apiSuccess {Number} data.port HttpMocker port.
 * @apiSuccess {String} message Success message.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "data": {
 *         "port": 0000
 *       }
 *     }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "status": "error",
 *       "message": "no available port"
 *     }
 */

export default class CreateHttpMockerHandler {
  readonly hostname: string
  private portsRange: number[]
  private usedPorts: number[] = []

  constructor (hostname: string, portsRange: number[]) {
    this.hostname = hostname
    this.portsRange = portsRange
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return this.createHttpMocker()
  }

  createHttpMocker (): Promise<IResponse> {
    return new Promise(async (resolve) => {
      let port = getPort(this.portsRange, this.usedPorts)
      if (!port) {
        return resolve({
          code: 500,
          body: getJsend({ statusCode: 500, data: undefined, message: 'no available port' })
        })
      }
      const mocker = new HttpMocker(this.hostname, port)
      mocker.loadServer()
      return mocker.runServer().then(() => {
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/routes`, method: GET },
          response: RoutesGetterHandler.prototype.handle.bind(new RoutesGetterHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/route`, method: GET },
          response: RouteGetterHandler.prototype.handle.bind(new RouteGetterHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/route`, method: POST },
          response: CreateRouteHandler.prototype.handle.bind(new CreateRouteHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/route`, method: PATCH },
          response: UpdateRouteHandler.prototype.handle.bind(new UpdateRouteHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/ws-route`, method: POST },
          response: CreateWsRouteHandler.prototype.handle.bind(new CreateWsRouteHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/ws-route`, method: DELETE },
          response: DeleteWsRouteHandler.prototype.handle.bind(new DeleteWsRouteHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/route`, method: DELETE },
          response: DeleteRouteHandler.prototype.handle.bind(new DeleteRouteHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/history`, method: GET },
          response: HistoryGetterHandler.prototype.handle.bind(new HistoryGetterHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: `/${RESERVED_PATH}/history`, method: DELETE },
          response: ClearHistoryHandler.prototype.handle.bind(new ClearHistoryHandler(mocker))
        })
        mocker.addRoute({
          filters: { path: '/', method: GET },
          response: MockerHealthCheckerHandler.prototype.handle.bind(new MockerHealthCheckerHandler())
        })
        mocker.addRoute({
          filters: { path: '/', method: DELETE },
          response: StopMockerHandler.prototype.handle.bind(new StopMockerHandler(mocker, CreateHttpMockerHandler.prototype.removePort.bind(this)))
        })
        this.usedPorts.push(port)
        let mockerInfoResponse = { port: port }
        return resolve({
          code: 200, body: getJsend({
            statusCode: 200,
            data: JSON.stringify(mockerInfoResponse),
            message: 'mocker successfully created'
          })
        })
      }).catch(() => {
        console.log(`Retrying to connect in another port, ${port} is in use.`)
        this.usedPorts.push(port)
        return resolve(this.createHttpMocker())
      })
    })
  }

  private removePort (mockerPort: number) {
    this.usedPorts = this.usedPorts.filter(port => {
      return port != mockerPort
    })
  }
}