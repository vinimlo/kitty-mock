import IResponse from '../interfaces/IResponse'
import IFilter from '../interfaces/IFilter'

export default function checkCreateWsRouteRequest (filters: IFilter, response: IResponse): string | undefined {
  if (filters == undefined) {
    return 'request missing filters'
  } else if (filters.path == undefined) {
    return 'request missing route path'
  } else if (!checkPath(filters.path)) {
    return 'request with invalid route path'
  }
}

function checkPath (path: string): boolean {
  let re = /^\/[-_\w\d\/]*/
  return re.test(path)
}
