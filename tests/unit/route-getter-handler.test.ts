import { expect } from 'chai'
import 'mocha'
import IResponse from '../../interfaces/IResponse'
import axios from 'axios'
import { createServer, Server } from 'http'
import IJsend from '../../interfaces/IJsend'
import HttpMocker from '../../mocker/httpMocker'
import IRoute from '../../interfaces/IRoute'
import RouteGetterHandler from '../../handlers/route-getter-handler'

describe('Route getter handler', () => {

  it('gets an existing route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new RouteGetterHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', '{"filters":{"path":"/oi","method":"GET"},"response":{"code":200,"body":"oioi"}}', undefined, 200)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    return axios.get('http://127.0.0.1:7003?path=/oi&method=get').finally(async () => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
  it('gets a non existing route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new RouteGetterHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'fail', undefined, 'route does not exist', 404)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    return axios.get('http://127.0.0.1:7003?path=/oi&method=get').finally(async () => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
})

async function checkResponse (response: IResponse, status: string, data: string, message: string, code: number) {
  let jsend: IJsend = JSON.parse(response.body)
  expect(jsend.status).to.equal(status)
  expect(JSON.stringify(jsend.data)).to.equal(data)
  expect(jsend.message).to.equal(message)
  expect(response.code).to.equal(code)
}

function getMockRoute (): IRoute {
  return {
    filters: { path: '/oi', method: 'GET' },
    response: { code: 200, body: 'oioi' }
  }
}
