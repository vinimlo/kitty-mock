import { expect } from 'chai'
import 'mocha'
import axios from 'axios'
import { createServer, Server } from 'http'
import IRoute from '../../interfaces/IRoute'
import passRequestThroughValitador from '../../helpers/pass-request-through-valitador'
import { POST } from '../../consts/methods-consts'
import IValidatorResponse from '../../interfaces/IValidatorResponse'

describe('Create route handler', () => {

  it('checks valid body on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('oi', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios.post('http://127.0.0.1:5003', 'oi').finally(() => {
      expect(validatorResponse.ok).to.be.true
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks invalid body on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('oii', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios.post('http://127.0.0.1:5003', 'oi').finally(() => {
      expect(validatorResponse.ok).to.be.false
      expect(validatorResponse.message).to.equal('Expected "oii" to be equal to "oi"')
      return server.close()
    })
  })
  it('checks valid object body on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: undefined, body: '{oi: "oi"}' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios.post('http://127.0.0.1:5003').finally(() => {
      expect(validatorResponse.ok).to.be.equal
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks valid one header key with string value on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: { 'token': '123' }, body: '{oi: "oi"}' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios({
      method: 'post',
      url: 'http://127.0.0.1:5003',
      headers: { 'token': '123' }
    }).finally(() => {
      expect(validatorResponse.ok).to.be.true
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks valid two header keys with string value on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{
        matchers: { header: { 'token': '123', 'token2': '123' }, body: '{oi: "oi"}' },
        code: 200,
        body: ''
      }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios({
      method: 'post',
      url: 'http://127.0.0.1:5003',
      headers: { 'token': '123', 'token2': '123' }
    }).finally(() => {
      expect(validatorResponse.ok).to.be.true
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks valid one header key with array value on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: { 'token': ['123'] }, body: '{oi: "oi"}' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios({
      method: 'post',
      url: 'http://127.0.0.1:5003',
      headers: { 'token': '123' }
    }).finally(() => {
      expect(validatorResponse.ok).to.be.true
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks valid one header key with array with two value on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: { 'token': ['123', '153'] }, body: '{oi: "oi"}' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios({
      method: 'post',
      url: 'http://127.0.0.1:5003',
      headers: { 'token': '123' }
    }).finally(() => {
      expect(validatorResponse.ok).to.be.true
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks valid two header key with array and string value on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{
        matchers: { header: { 'token': ['123', '153'], 'token2': '123' }, body: '{oi: "oi"}' },
        code: 200,
        body: ''
      }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios({
      method: 'post',
      url: 'http://127.0.0.1:5003',
      headers: { 'token': '123', 'token2': '123' }
    }).finally(() => {
      expect(validatorResponse.ok).to.be.true
      expect(validatorResponse.message).to.be.undefined
      return server.close()
    })
  })
  it('checks invalid one header key with array value on validator', () => {
    let validatorResponse: IValidatorResponse
    let route: IRoute = {
      filters: { path: '/53345sd', method: POST },
      response: { code: 200, body: 'json' },
      validator: [{ matchers: { header: { 'token': ['1253', '153'] }, body: '{oi: "oi"}' }, code: 200, body: '' }]
    }
    let server: Server = createServer(async (req, res) => {
      validatorResponse = passRequestThroughValitador('{oi: "oi"}', req, route.validator[0])
      res.statusCode = 200
      res.end()
    })
    server.listen(5003)
    return axios({
      method: 'post',
      url: 'http://127.0.0.1:5003',
      headers: { 'token': '123' }
    }).finally(() => {
      expect(validatorResponse.ok).to.be.false
      expect(validatorResponse.message).to.equal('Expected header token to be equal to one of [1253, 153].')
      return server.close()
    })
  })
})
