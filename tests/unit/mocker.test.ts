import { expect } from 'chai'
import 'mocha'
import HttpMocker from '../../mocker/httpMocker'
import axios from 'axios'
import IRoute from '../../interfaces/IRoute'

describe('Mocker', () => {

  it('creates route', async () => {
    const mocker = new HttpMocker('127.0.0.1', 5008)
    mocker.loadServer()
    await mocker.runServer()
    mocker.addRoute(getMockRoute())
    let responseData: string = ''
    await axios.get('http://127.0.0.1:5008/oi')
      .then((response) => {
        responseData = JSON.stringify(response.data)
      })
    expect(responseData).to.equal('"oioi"')
    return mocker.stopServer()
  })
  it('creates server', async () => {
    const mocker = new HttpMocker('127.0.0.1', 5009)
    mocker.loadServer()
    await mocker.runServer()
    mocker.addRoute(getMockRoute())
    await mocker.stopServer()
    let success: number = 0
    let failed: number = 0
    await axios.get('http://127.0.0.1:5009/oioi')
      .then(() => success++)
      .catch(() => failed++)
    expect(failed).to.equal(1)
    expect(success).to.equal(0)
  })

})

function getMockRoute (): IRoute {
  return {
    filters: { path: '/oi', method: 'GET' },
    response: { code: 200, body: 'oioi' }
  }
}