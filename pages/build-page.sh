#!/bin/sh -e

MD="$1"
TITLE="$2"

if test -z "$MD"; then
  echo '
  You must provide an `*.md` file as the first parameter.
  You can optionaly provide a title as the second parameter.
  ' >&2
  exit 1
fi

if test -z "$TITLE"; then
  TITLE=$(egrep -B1 '==+' $MD | head -n1)
fi

cat <<EOF
<!doctype html>
<html>
<head>
  <meta charset="utf-8"/>
  <title>$TITLE</title>
  <link href="./style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <div id="content">$(marked $MD)</div>
</body>
</html>
EOF
