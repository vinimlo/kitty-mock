import { expect } from 'chai'
import 'mocha'
import IResponse from '../../interfaces/IResponse'
import axios from 'axios'
import { createServer, Server } from 'http'
import IJsend from '../../interfaces/IJsend'
import HistoryGetterHandler from '../../handlers/history-getter-handler'
import IRequest from '../../interfaces/IRequest'
import IRoute from '../../interfaces/IRoute'
import HttpMocker from '../../mocker/httpMocker'

describe('History getter handler', () => {

  it('gets history with request', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRequestShelf().setRequest('GET/oi', getMockRequest())
    mocker.getRouteShelf().setItem(getMockRoute('/oi', 'GET'))
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new HistoryGetterHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', '[{"ip":"127.0.0.1","header":{},"body":"oi","method":"GET","url":"/oi","date":"data"}]', undefined, 200)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7053)
    return axios.post('http://127.0.0.1:7053?path=/oi&method=get').finally(() => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
  it('gets history no request', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute('/oi', 'GET'))
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new HistoryGetterHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', '[]', undefined, 200)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7054)
    return axios.post('http://127.0.0.1:7054?path=/oi&method=get').finally(() => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
  it('gets history with request but not for the required', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute('/oii', 'GET'))
    let success: number = 0
    mocker.getRequestShelf().setRequest('GET/oi', getMockRequest())
    let server: Server = createServer(async (req, res) => {
      new HistoryGetterHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', '[]', undefined, 200)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7055)
    return axios.post('http://127.0.0.1:7055?path=/oii&method=get').finally(() => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
  it('gets history from a non existent route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute('/oi', 'GET'))
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new HistoryGetterHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'fail', undefined, 'route does not exist', 404)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7056)
    return axios.post('http://127.0.0.1:7056?path=/oii&method=get').finally(() => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
})

function checkResponse (response: IResponse, status: string, data: string, message: string, code: number) {
  let jsend: IJsend = JSON.parse(response.body)
  expect(jsend.status).to.equal(status)
  expect(JSON.stringify(jsend.data)).to.equal(data)
  expect(jsend.message).to.equal(message)
  expect(response.code).to.equal(code)
}

function getMockRequest (): IRequest {
  return {
    ip: '127.0.0.1',
    header: {
      cookie: undefined,
      authorization: undefined,
      connection: undefined,
      contentType: undefined,
      contentLanguage: undefined
    },
    body: 'oi',
    method: 'GET',
    url: '/oi',
    date: 'data'
  }
}

function getMockRoute (path: string, method: string): IRoute {
  return {
    filters: { path: path, method: method },
    response: { code: 200, body: 'oioi' }
  }
}

