import IWs from '../interfaces/IWs'
import IWsShelf from '../interfaces/IWsShelf'
import * as WebSocket from 'ws'
import IWsParams from '../interfaces/IWsParams'

export default class WsShelf implements IWsShelf {
  private wsList: IWs[] = []

  public getWs (path: string): IWs {
    return this.findWsByRoutePath(path)
  }

  public setWs (webSocket: IWs): boolean {
    let ws: IWs = this.findWsByRoutePath(webSocket.routeWs.path)
    if (ws) {
      return false
    }
    this.wsList.push(webSocket)
    return true
  }

  public deleteWs (path: string): boolean {
    let ws: IWs = this.findWsByRoutePath(path)
    if (ws) {
      this.wsList = this.filterByPath(path)
      return true
    }
    return false
  }

  public trySendAllPendingMessages (ws: IWsParams): void {
    if (!ws.socket) {
      return
    }
    while (ws.messages.length) {
      ws.socket.send(ws.messages.shift())
    }
  }

  public tryToSendMessageOrStorage (ws: IWsParams, msg: WebSocket.Data) {
    if (ws.socket) {
      ws.socket.send(msg)
    } else {
      ws.messages.push(msg)
    }
  }

  private findWsByRoutePath (path: string): IWs {
    return this.wsList.find((ws) => ws.routeWs.path == path || ws.routePairWs.path == path)
  }

  private filterByPath (path: string): IWs[] {
    return this.wsList.filter((ws) => ws.routeWs.path != path)
  }
}

