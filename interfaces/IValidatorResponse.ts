export default interface IValidatorResponse {
  ok: boolean,
  message?: string
}
