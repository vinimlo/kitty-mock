export function checkQuery (query: any): string {
  if (!query.path) {
    return 'request query missing path'
  } else if (!query.method) {
    return 'request query missing method'
  }
  return ''
}