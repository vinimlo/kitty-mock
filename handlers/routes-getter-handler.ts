import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import { IncomingMessage } from 'http'
import IRoute from '../interfaces/IRoute'
import IFilter from '../interfaces/IFilter'
import { RESERVED_PATH } from '../consts/kitty'
import IHttpMocker from '../interfaces/IHttpMocker'

/**
 * @api {get} :port/=^.^=/routes Get HttpMocker routes

 * @apiGroup HttpMocker
 * @apiVersion 0.4.0
 * @apiName GetRoutes
 * @apiDescription It gets a HttpMocker routes.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} data An array with routes.
 * @apiSuccess {Object} data.filters         The route filters.
 * @apiSuccess {String} data.filters.path    The route path. If with query, must be encoded.
 * @apiSuccess {String} data.filters.method  The route method.
 * @apiSuccess {Object} data.response        The route response.
 * @apiSuccess {Number} data.response.code   The route response code. It musts be between 100 and 600.
 * @apiSuccess {String} data.response.body   The route response body.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *       "status": "success",
 *       "data": [
 *        {
 *         "filters":{
 *           "path": "/oi",
 *           "method": "GET"
 *         },
 *         "response":{
 *           "body": "test",
 *           "code": 200
 *         }
 *        }
 *       ]
 *     }
 */

export default class RoutesGetterHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
      let routes: IRoute[] | undefined = this.mocker.getRouteShelf().getItems()
      resolve({
        code: 200,
        body: getJsend({ statusCode: 200, data: JSON.stringify(hydrate(routes)), message: undefined })
      })
    })
  }
}

function hydrate (routes: IRoute[]): IFilter[] {
  let filteredRoutes: IRoute[] = routes.filter((route) => {
    return !route.filters.path.includes(`${RESERVED_PATH}`) && route.filters.path != `/`
  })
  return filteredRoutes.map((route) => {
    return route.filters
  })
}