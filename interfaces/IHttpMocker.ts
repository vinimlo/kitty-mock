import IRoute from './IRoute'
import IRouteShelf from './IRouteShelf'
import IRequestShelf from './IRequestShelf'
import { Server } from 'http'
import IWsShelf from './IWsShelf'

export default interface IHttpMocker {
  loadServer (): void

  runServer (): Promise<string>

  addRoute (route: IRoute): void

  stopServer (): Promise<string | undefined>

  getRouteShelf (): IRouteShelf

  getRequestShelf (): IRequestShelf

  getWsShelf (): IWsShelf

  getServerInstance (): Server
}