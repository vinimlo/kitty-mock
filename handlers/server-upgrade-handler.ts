import { IncomingMessage } from 'http'
import { Socket } from 'net'
import * as url from 'url'
import IWsShelf from '../interfaces/IWsShelf'
import IWs from '../interfaces/IWs'
import IWsParams from '../interfaces/IWsParams'
import { printConnected, printDisconnected, printWsLog } from '../helpers/print-functions'

export default class CreateWsRouteHandler {
  private request: IncomingMessage
  private socket: Socket
  private head: Buffer
  private wsShelf: IWsShelf

  constructor (wsShelf: IWsShelf, request: IncomingMessage, socket: Socket, head: Buffer) {
    this.request = request
    this.socket = socket
    this.head = head
    this.wsShelf = wsShelf
  }

  public handle () {
    const pathname = url.parse(this.request.url).pathname
    let ws: IWs = this.wsShelf.getWs(pathname)
    if (ws && pathname == ws.routeWs.path) {
      this.handleUpgradeListenEventsAndSetSocket(ws.routeWs, ws.routePairWs)
    } else if (ws && pathname == ws.routePairWs.path) {
      this.handleUpgradeListenEventsAndSetSocket(ws.routePairWs, ws.routeWs)
    } else {
      this.socket.destroy()
    }
  }

  public handleUpgradeListenEventsAndSetSocket (ws: IWsParams, wsPair: IWsParams) {
    ws.server.handleUpgrade(this.request, this.socket, this.head, (socket) => {
      printConnected(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), ws.path)
      ws.socket = socket
      this.wsShelf.trySendAllPendingMessages(ws)
      socket.on('message', (msg) => {
        printWsLog(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), ws.path, msg)
        this.wsShelf.tryToSendMessageOrStorage(wsPair, msg)
      })
      socket.on('close', () => {
        printDisconnected(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), ws.path)
        ws.socket = undefined
      })
    })
  }

}




