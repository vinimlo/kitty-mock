export default interface IQuery {
  path: string,
  method: string
}
