import { expect } from 'chai'
import 'mocha'
import IResponse from '../../interfaces/IResponse'
import axios from 'axios'
import { createServer, Server } from 'http'
import IRoute from '../../interfaces/IRoute'
import DeleteRouteHandler from '../../handlers/delete-route-handler'
import IJsend from '../../interfaces/IJsend'
import HttpMocker from '../../mocker/httpMocker'

describe('Delete route handler', () => {
  it('deletes an existent route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    let success: number = 0
    let failed: number = 0
    const handler = new DeleteRouteHandler(mocker)
    let server: Server = createServer((req, res) => {
      handler.handle(req).then((response) => {
        checkResponse(response, 'success', undefined, 204)
        success++
      }).catch(() => failed++)
      res.statusCode = 200
      res.end()
    })
    server.listen(5019)
    return axios.delete('http://127.0.0.1:5019?path=/oi&method=GET').finally(() => {
      expect(success).to.equal(1)
      expect(failed).to.equal(0)
      return server.close()
    })
  })
  it('deletes a inexistent route', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    let success: number = 0
    let failed: number = 0
    const handler = new DeleteRouteHandler(mocker)
    let server: Server = createServer((req, res) => {
      handler.handle(req).then((response) => {
        checkResponse(response, 'fail', 'route does not exist', 400)
        success++
      }).catch(() => failed++)
      res.statusCode = 200
      res.end()
    })
    server.listen(5020)
    return axios.delete('http://127.0.0.1:5020?path=/oi&method=GET').finally(() => {
      expect(success).to.equal(1)
      expect(failed).to.equal(0)
      return server.close()
    })
  })
  it('deletes route with no query', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    let success: number = 0
    const handler = new DeleteRouteHandler(mocker)
    let server: Server = createServer((req, res) => {
      handler.handle(req).then((response) => {
        checkResponse(response, 'fail', 'request query missing path', 400)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(5021)
    return axios.delete('http://127.0.0.1:5021').finally(() => {
      expect(success).to.equal(1)
      return server.close()
    })
  })
})

function checkResponse (response: IResponse, status: string, message: string, code: number) {
  let jsend: IJsend = JSON.parse(response.body)
  expect(jsend.status).to.equal(status)
  expect(jsend.data).to.equal(undefined)
  expect(jsend.message).to.equal(message)
  expect(response.code).to.equal(code)
}

function getMockRoute (): IRoute {
  return {
    filters: { path: '/oi', method: 'GET' },
    response: { code: 200, body: 'oioi' }
  }
}