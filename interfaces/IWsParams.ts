import * as WebSocket from 'ws'

export default interface IWsParams {
  path: string
  server: WebSocket.Server,
  socket: WebSocket
  messages: WebSocket.Data[]
}