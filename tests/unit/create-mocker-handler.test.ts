import 'mocha'
import { expect } from 'chai'
import IResponse from '../../interfaces/IResponse'
import CreateHttpMockerHandler from '../../handlers/create-http-mocker-handler'
import axios from 'axios'
import IJsend from '../../interfaces/IJsend'

describe('Testing create mocker handler', () => {

  it('creates a mocker', async () => {
    let response: IResponse = await new CreateHttpMockerHandler('0.0.0.0', [5006, 5010]).handle(undefined)
    expect(response.code).to.equal(200)
    let jsend: IJsend = JSON.parse(response.body)
    expect(jsend.status).to.equal('success')
    expect(jsend.message).to.equal('mocker successfully created')
    let data = JSON.parse(jsend.data)
    expect(data.port >= 5006 && data.port < 5011).to.be.true
    await checkMockerStatus(data.port)
    await deleteMocker(data.port)
  })
  it('deletes a mocker', async () => {
    let response: IResponse = await new CreateHttpMockerHandler('0.0.0.0', [6010, 6014]).handle(undefined)
    expect(response.code).to.equal(200)
    let jsend: IJsend = JSON.parse(response.body)
    expect(jsend.status).to.equal('success')
    expect(jsend.message).to.equal('mocker successfully created')
    expect(jsend.data).not.undefined
    let data = JSON.parse(jsend.data)
    await deleteMocker(data.port)
  })
})

function deleteMocker (port: string) {
  return axios.delete('http://localhost:' + port + '/').then((response) => {
    expect(response.status).to.equal(204)
  })
}

function checkMockerStatus (port: string) {
  return axios.get('http://localhost:' + port + '/').then((response) => {
    expect(response.status).to.equal(204)
    expect(response.data).to.equal('')
  })
}