import IResponse from '../interfaces/IResponse'
import IRoute from '../interfaces/IRoute'

export default function checkUpdateRouteRequest (route: IRoute): string | undefined {
  route.response = route.response as IResponse
  if (route.filters)
    return 'filters cannot be updated'

  if (!route.validator || route.validator.length == 0) {
    if (!route.response)
      return 'request missing route response'
    else if (!route.response.code)
      return 'request missing route response code'
    else if (!checkCode(route.response.code))
      return 'request with invalid route response code'
    else if (route.response.body == undefined)
      return 'request missing route response body'
  } else {
    let err: string = ''
    for (let i = 0; i < route.validator.length; i++)
      if (route.validator[i].matchers.body == undefined && !route.validator[i].matchers.header) {
        err = 'request missing validator matchers'
        break
      } else if (!checkCode(route.validator[i].code)) {
        err = 'request with invalid route validator code'
        break
      }
    if (err) return err
  }
}

function checkCode (code: number): boolean {
  return code >= 100 && code < 600
}