Kitty Mock
==========
**An API mock server**

<big>[&rarr; go to the docs!](https://ntopus.gitlab.io/kitty-mock/docs)</big>

## Change Log

### 0.1.0
#### Added
- Create mocker;
- Create route;
- Delete route ;
- Check route history;
- Clean route history;
- Delete mocker;
- Check mocker route list;
- Check Kitty Mock health.

### 0.1.3

#### Fixed

- Sending duplicated params bug.

### 0.1.4

#### Fixed

- Cleaning mocker route list, tanking off default routes.

### 0.1.5

#### Fixed

- Delete route bug;

### 0.1.6

#### Fixed

- Bugs.

### 0.2.0

#### Added

- WebSocket route creation and deletion.

### 0.3.0

#### Added

- Request body validator.

### 0.3.1

#### Fixed

- Browser made request bug.

### 0.3.2

#### Fixed

- Validator definition.

### 0.4.0

#### Added

- Route update.