import { IncomingMessage } from 'http'
import IValidatorResponse from '../interfaces/IValidatorResponse'
import IValidator from '../interfaces/IValidator'

function fail (message: string): IValidatorResponse {
  return { ok: false, message }
}

export default function passRequestThroughValitador (body: string, req: IncomingMessage, validator: IValidator): IValidatorResponse {
  if (validator.matchers.body) {
    if (!(body == validator.matchers.body)) return fail(`Expected "${body}" to be equal to "${validator.matchers.body}"`)
  }
  if (!!validator.matchers.header) {
    const vHeaderKeyList: string[] = Object.keys(validator.matchers.header)
    let vHeaderKey: string
    for (let i = 0; i < vHeaderKeyList.length; i++) {
      vHeaderKey = vHeaderKeyList[i]
      const headerMatcher = validator.matchers.header[vHeaderKey]
      if (typeof (headerMatcher) === 'string') {
        if (req.headers[vHeaderKey] !== headerMatcher) return fail(`Expected header ${vHeaderKey} to be equal to ${headerMatcher}.`)
      } else if (headerMatcher instanceof Array) {
        if (!headerMatcher.map(m => req.headers[vHeaderKey] === m).find(val => val)) {
          return fail(`Expected header ${vHeaderKey} to be equal to one of [${headerMatcher.join(', ')}].`)
        }
      } else {
        throw Error(`The type of (${typeof headerMatcher}) of the header matcher for ${vHeaderKey} is not valid.`)
      }
    }
  }
  return { ok: true }
}