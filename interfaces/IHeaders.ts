export default interface IHeaders {
  cookie: string
  authorization: string
  connection: string
  contentType: string
  contentLanguage: string
}


