import IRoute from './IRoute'

export default interface IRouteShelf {
  getItems (): IRoute[]

  getItem (path: string, method: string): Promise<IRoute>

  setItem (route: IRoute): boolean

  updateItem (routePath: string, routeMethod: string, route: IRoute): boolean

  removeItem (routePath: string, routeMethod: string): boolean

}