import getJsend from '../helpers/get-jsend'
import getRequestBody from '../helpers/get-request-body'
import { IncomingMessage } from 'http'
import IRoute from '../interfaces/IRoute'
import IResponse from '../interfaces/IResponse'
import IHttpMocker from '../interfaces/IHttpMocker'
import checkUpdateRouteRequest from '../helpers/check-update-route-request'
import IQueryResponse from '../interfaces/IQueryResponse'
import getAndCheckQuery from '../helpers/get-and-check-query'

/**
 * @api {patch} :port/=^.^=/route Update a route

 * @apiGroup HttpMocker
 * @apiVersion 0.4.0
 * @apiName UpdateRoute
 * @apiDescription It updates a route in HttpMocker
 *
 * @apiParam {String} path    The route path. It musts be a query param. If with query, must be encoded.
 * @apiParam {String} method  The route method. It musts be a query param.
 *
 * @apiParam {Object} [response]        The route response. It musts be set if does not exist a validator.
 * @apiParam {Number} response.code   The route response code. It musts be between 100 and 600.
 * @apiParam {String} response.body   The route response body.
 * @apiParam {Object[]} [validator]   The route request validator.
 * @apiParam {Object} validator.matchers   The validator matcher.
 * @apiParam {String||Object} validator.matchers.body   The validator body matcher.
 * @apiParam {String[String||Object]} validator.matchers.header   The validator header matcher. A object map with string keys and string or objects values.
 * @apiParam {String} validator.code   The code response when validator matched body.
 * @apiParam {String||Object} validator.body   The body response when validator matched body.
 *
 * @apiSuccess {String} status Request status
 * @apiSuccess {String} message Success message
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "message": "route updated successfully"
 *     }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Internal Server Error
 *     {
 *        "status": "fail",
 *        "message": "route does not exist"
 *     }
 */
export default class UpdateRouteHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return getRequestBody(req).then((body) => {
      let response: IQueryResponse = getAndCheckQuery(req)
      if (response.err)
        return {
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: response.err })
        }
      response.query.path = decodeURIComponent(response.query.path)
      let route: IRoute = JSON.parse(body)
      route.response = route.response as IResponse
      let err: string = checkUpdateRouteRequest(route)
      if (err) return { code: 400, body: getJsend({ statusCode: 400, data: undefined, message: err }) }
      if (route.response)
        route.response.body = typeof route.response.body != 'string' ? JSON.stringify(route.response.body) : route.response.body
      if (route.validator)
        for (let i = 0; i < route.validator.length; i++)
          if (route.validator && route.validator[i].matchers.body)
            route.validator[i].matchers.body = typeof route.validator[i].matchers.body != 'string' ? JSON.stringify(route.validator[i].matchers.body) : route.validator[i].matchers.body
      if (this.mocker.getRouteShelf().updateItem(response.query.path, response.query.method, route)) {
        return {
          code: 204,
          body: getJsend({ statusCode: 204, data: undefined, message: undefined })
        }
      } else {
        return {
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: 'route does not exist' })
        }
      }
    }).catch((err) => {
      return {
        code: 400,
        body: getJsend({ statusCode: 400, data: undefined, message: 'request missing body. ' + err.message })
      }
    })
  }
}


