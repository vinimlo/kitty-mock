import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import { IncomingMessage } from 'http'
import IQuery from '../interfaces/IQuery'
import { checkQuery } from '../helpers/check-query'
import { treatQuery } from '../helpers/treatQuery'
import { ParsedUrlQuery } from "querystring"
import {parse} from 'url'
import IHttpMocker from '../interfaces/IHttpMocker'

export default class DeleteHistoryHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
      let queryObject: ParsedUrlQuery = parse(req.url, true).query
      let err: string = checkQuery(queryObject)
      if (err) {
        return resolve({ code: 500, body: getJsend({ statusCode: 500, data: undefined, message: err }) })
      }
      let query: IQuery = treatQuery(queryObject)
      this.mocker.getRequestShelf().deleteRequests(query.method.toUpperCase() + query.path)
      resolve({
        code: 204,
        body: getJsend({ statusCode: 204, data: undefined, message: undefined })
      })
    })
  }
}
