import { IncomingMessage } from 'http'
import IHttpMocker from '../interfaces/IHttpMocker'
import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import IRemovePort from '../interfaces/IRemovePort'

/**
 * @api {delete} :port/ Stop a HttpMocker
 * @apiVersion 0.4.0
 * @apiGroup HttpMocker
 * @apiName StopMocker
 * @apiDescription It stops a HttpMocker.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 */

export default class StopMockerHandler {
  private mocker: IHttpMocker
  private removePort: IRemovePort

  constructor (mocker: IHttpMocker, removePort: IRemovePort) {
    this.mocker = mocker
    this.removePort = removePort
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
      this.removePort(req.socket.localPort)
      resolve({ code: 204, body: getJsend({ statusCode: 204, data: undefined, message: undefined }) })
      this.mocker.stopServer()
    })
  }
}



