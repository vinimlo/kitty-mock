import IRouteShelf from '../interfaces/IRouteShelf'
import { createServer, IncomingMessage, Server, ServerResponse } from 'http'
import IRoute from '../interfaces/IRoute'
import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import IHttpMocker from '../interfaces/IHttpMocker'
import { performance } from 'perf_hooks'
import IHandler from '../interfaces/IHandler'
import IRequestShelf from '../interfaces/IRequestShelf'
import IRequest from '../interfaces/IRequest'
import getRequestBody from '../helpers/get-request-body'
import RouteShelf from '../routeShelf/route-shelf'
import RequestShelf from '../requestShelf/request-shelf'
import IWsShelf from '../interfaces/IWsShelf'
import WsShelf from '../wsShelf/ws-shelf'
import CreateWsRouteHandler from '../handlers/server-upgrade-handler'
import { printRequestsLog } from '../helpers/print-functions'
import passRequestThroughValitador from '../helpers/pass-request-through-valitador'
import { OPTIONS } from '../consts/methods-consts'
import IValidatorResponse from '../interfaces/IValidatorResponse'
import IValidator from '../interfaces/IValidator'
import ErrnoException = NodeJS.ErrnoException

export default class HttpMocker implements IHttpMocker {
  readonly routeShelf: IRouteShelf
  readonly requestShelf: IRequestShelf
  readonly wsShelf: IWsShelf
  private port: number
  private server: Server
  private hostname: string

  constructor (hostname: string, port: number) {
    this.routeShelf = new RouteShelf()
    this.port = port
    this.hostname = hostname
    this.requestShelf = new RequestShelf()
    this.wsShelf = new WsShelf()
  }

  public getRouteShelf (): IRouteShelf {
    return this.routeShelf
  }

  public getWsShelf (): IWsShelf {
    return this.wsShelf
  }

  public getRequestShelf (): IRequestShelf {
    return this.requestShelf
  }

  public getServerInstance (): Server {
    return this.server
  }

  public loadServer (): void {
    this.server = createServer((req, res) => {
      let initialTime: number = performance.now()
      let ok: boolean = req.url.includes('/=%5E.%5E=/')
      let path: string = ok ? req.url.split('?', 1)[0] : req.url
      this.routeShelf.getItem(path, req.method).then(async route => {
        if (typeof route.response != 'function') {
          let response: IResponse = route.response as IResponse
          if (route.validator)
            getRequestBody(req).then(body => {
              let defaultValidator: IValidator = route.validator.find(validator => validator.matchers.body == '*')
              let validatorsWithoutDefault: IValidator[] = route.validator.filter(valitator => valitator.matchers.body != '*')
              for (let i = 0; i < validatorsWithoutDefault.length; i++) {
                try {
                  let validatorResponse: IValidatorResponse = passRequestThroughValitador(body, req, validatorsWithoutDefault[i])
                  if (validatorResponse.ok) {
                    this.respRequest(res, validatorsWithoutDefault[i].code, validatorsWithoutDefault[i].body, req.method, req.url, initialTime)
                    break
                  }
                } catch (validatorResponse) {
                  this.respRequest(res, 500, validatorResponse.message, req.method, req.url, initialTime)
                  break
                }
                if (i == validatorsWithoutDefault.length - 1)
                  if (defaultValidator)
                    this.respRequest(res, defaultValidator.code, defaultValidator.body, req.method, req.url, initialTime)
                  else
                    this.respRequest(res, 404, undefined, req.method, req.url, initialTime)
              }
            }).catch(() => {
              this.respRequest(res, 500, getJsend({
                statusCode: 500,
                data: undefined,
                message: undefined
              }), req.method, req.url, initialTime)
            })
          else
            this.respRequest(res, response.code, response.body, req.method, req.url, initialTime)
        } else {
          let handle: IHandler = route.response as IHandler
          return handle(req).then((resp: IResponse) => {
            this.respRequest(res, resp.code, resp.body, req.method, req.url, initialTime)
          })
        }
        this.hydrateRequest(req).then(request => {
          this.requestShelf.setRequest(req.method.toUpperCase() + req.url, request)
        })
      }).catch((code) => {
        if (req.method.toLocaleUpperCase() == OPTIONS && code == 405) {
          this.respRequest(res, 204, undefined, req.method, req.url, initialTime)
          return
        }
        if (typeof code == 'number')
          this.respRequest(res, code, getJsend({
            statusCode: code,
            data: undefined,
            message: undefined
          }), req.method, req.url, initialTime)
        else
          this.respRequest(res, 500, getJsend({
            statusCode: 500,
            data: undefined,
            message: undefined
          }), req.method, req.url, initialTime)
      })
    })
    this.server.on('upgrade', (request, socket, head) => {
      new CreateWsRouteHandler(this.wsShelf, request, socket, head).handle()
    })
  }

  public runServer (): Promise<string> {
    return new Promise((resolve, reject) => {
      this.server.listen(Number(this.port), this.hostname, () => {
        resolve(`New mocker running at http://${this.hostname}:${this.port}/`)
      })
      this.server.on('error', (e: ErrnoException) => {
        if (e.code === 'EADDRINUSE')
          reject(`Address ${this.hostname}:${this.port} already in use...`)
      })
    })
  }

  public addRoute (route: IRoute): void {
    console.log('New route added to mocker on port ' + this.port + ' | ' + ' '.repeat(7 - route.filters.method.length) + route.filters.method + ' ' + route.filters.path)
    this.routeShelf.setItem(route)

  }

  public stopServer (): Promise<string | undefined> {
    console.log('Closing mocker on port ' + this.port)
    return new Promise((resolve, reject) =>
      this.server.close((error) => error ? reject(error) : resolve(undefined))
    )
  }

  private respRequest (res: ServerResponse, code: number, body: any, method: string, url: string, initialTime: number): void {
    res.writeHead(code, {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*'
    })
    res.end(body, () => {
      printRequestsLog(code, method, url, initialTime)
    })
  }

  private async hydrateRequest (req: IncomingMessage): Promise<IRequest> {
    let body: string = await getRequestBody(req)
    return {
      ip: req.socket.remoteAddress,
      header: {
        cookie: req.headers.cookie,
        authorization: req.headers.authorization,
        connection: req.headers.connection,
        contentType: req.headers['content-type'],
        contentLanguage: req.headers['content-language']
      },
      body: body,
      method: req.method,
      url: req.url,
      date: new Date().toString()
    }
  }
}